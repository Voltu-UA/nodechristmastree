/*
Your task is to build a custom Christmas tree with the specified characters and the specified height.

Inputs:
chars: the specified characters.
n: the specified height. A positive integer greater than 2.
Output:
A multiline string. Each line is separated by \n. A tree contains two parts: leaves and trunks.
The leaves should be n rows. The first row fill in 1 char, the second row fill in 3 chars, and so on. A single space will be added between two adjust chars, and some of the necessary spaces will be added to the left side, to keep the shape of the tree. No space need to be added to the right side.

The trunk should be at least 1 unit height, it depends on the value of the n. The minimum value of n is 3, and the height of the tree trunk is 1 unit height. If n increased by 3, and the tree trunk increased by 1 unit. For example, when n is 3,4 or 5, trunk should be 1 row; when n is 6,7 or 8, trunk should be 2 row; and so on.

Still not understand the task? Look at the following example ;-)

Examples
For chars = "*@o" and n = 3,the output should be:

  *
 @ o
* @ o
  |
For chars = "*@o" and n = 6,the output should be:

     *
    @ o
   * @ o
  * @ o *
 @ o * @ o
* @ o * @ o
     |
     |
For chars = "1234" and n = 6,the output should be:

     1
    2 3
   4 1 2
  3 4 1 2
 3 4 1 2 3
4 1 2 3 4 1
     |
     |
For chars = "123456789" and n = 3,the output should be:

  1
 2 3
4 5 6
  |
  
 */


function christmasTree(n, chars) {
  const midpoint = Math.floor((2 * n - 1) / 2);
  const trunkAmount = Math.floor(n / 3);

  // Realiziation of Queue data structure.
  class Queue {
    constructor() {
      this.data = [];
    }
  
    add(record) {
      this.data.push(record);
    }
  
    remove() {
      return this.data.shift();
    }
  };

  // Build queue object from incoming characters string.
  let splitedChars = chars.split('').join(' ').split('');
  // Add extra ' ', between last and first charachter of the queue for proper iteration.
  splitedChars.push(' ');
  let queue = new Queue();
  splitedChars.forEach((character) => {
    queue.add(character);
  });

  // Build a tree's leaves.
  for (let row = 0; row < n; row++) {
    let leavesLevel = '',
    leftSideSpaces = '',
    retrievedQueueItem;

    for (let column = 0; column < 2 * n - 1; column++) {
      if (midpoint - row <= column && midpoint + row >= column) {
        retrievedQueueItem = queue.remove();
        if (retrievedQueueItem === ' ' && leavesLevel === '') {
          queue.add(retrievedQueueItem);
          retrievedQueueItem = queue.remove();
          leavesLevel += retrievedQueueItem;
          queue.add(retrievedQueueItem);
        } else {
          leavesLevel += retrievedQueueItem;
          queue.add(retrievedQueueItem); 
        }
      } else if (midpoint - row > column) {
        leftSideSpaces += ' ';
      }
    }

    // Log tree's leaves.
    console.log(leftSideSpaces + leavesLevel);
  }

  // Build a tree's trunk.
  let trunkLevel = '';
  for (let i = 0; i < midpoint; i++) {
    trunkLevel += ' ';
  }

  trunkLevel = trunkLevel.split('');
  trunkLevel[midpoint] = '|';
  trunkLevel = trunkLevel.join('');

  // Log tree's trunk.
  for (let i = 0; i < trunkAmount; i++) {
    console.log(trunkLevel);
  }
}   

// Test
christmasTree(3, "*@o");
christmasTree(6, "*@o");
christmasTree(6, "1234");
christmasTree(3, "123456789");
